#!/bin/sh

if [ $# -eq 0 ]; then
	logger "netty-oom-handler: Please provide the pid"
	exit 2
fi

logger netty-oom-handler; Netty is out of memory, killing pid $1
kill $1