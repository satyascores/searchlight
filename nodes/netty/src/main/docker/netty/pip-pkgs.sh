#!/bin/bash

set -e

/usr/bin/pip install requests
/usr/bin/pip install cassandra-driver==3.0.0