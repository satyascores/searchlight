#!/usr/bin/python

import argparse, os

parser = argparse.ArgumentParser(description="Run netty.")

parser.add_argument('--conf', help="Specify the netty properties configuration file")
parser.add_argument('--heapDumpPath', help="Specify the location of the heap dump file on OOME")
parser.add_argument('--initialHeapSize', help="Specify JVM initial heap size")
parser.add_argument('--maxHeapSize', help="Specify JVM max heap size")
parser.add_argument('--jvmSystemProps', help="Specify JVM system props")

args = parser.parse_args()

propertiesFile = ""
heapDumpPath="./netty_heap_dump.hprof"
initialHeapSize="6g"
maxHeapSize="6g"
jvmSystemProps=""

if args.conf:
	propertiesFile = args.conf
else:
	print("netty configuration properties file required")
	exit(1)

if args.heapDumpPath:
	heapDumpPath = args.heapDumpPath

if args.initialHeapSize:
	initialHeapSize = args.initialHeapSize

if args.maxHeapSize:
	maxHeapSize = args.maxHeapSize

if args.jvmSystemProps:
	jvmSystemProps = args.jvmSystemProps

args = ["netty"]
args.append("-Xms{}".format(initialHeapSize))
args.append("-Xmx{}".format(maxHeapSize))
args.append("-XX:OnOutOfMemoryError='/apps/netty/netty-oom-handler.sh %p'")
args.append("-XX:+HeapDumpOnOutOfMemoryError")
if heapDumpPath:
	args.append("-XX:heapDumpPath={}".format(heapDumpPath))
if jvmSystemProps:
	args.append(jvmSystemProps.replace('+', '-'))
args.append("-cp")
args.append("/apps/netty/netty-1.0.0-SNAPSHOT/*")
args.append("com.bmc.geneva.gateway.server.Main")
args.append(propertiesFile)

#Start rsyslogd
os.system("/usr/sbin/rsyslogd")

# Start Netty
os.execv("/usr/bin/java", args)


