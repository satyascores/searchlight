#!/usr/bin/python

import argparse
import os
import jinja2

parser = argparse.ArgumentParser(description='Configure Cassandra Cluster.')

parser.add_argument('--clusterName', help='Sprecify a unique cassandra cluster name')
parser.add_argument('--seeds', help='Specify seeds with comma separated values if there are multiple values, default is 127.0.0.1')
parser.add_argument('--rpcPort', help='Specify initil rpc port. default is 9160')
parser.add_argument('--hostIP', help='Specify a unique cassandra local host name')
parser.add_argument('--dataDir', help='Specify C* data directory')
parser.add_argument('--logDir', help='Specify C* log directory')
parser.add_argument('--commitLogDir', help='Specify C* commit log directory')
parser.add_argument('--auto_bootstrap', help='Set the bootstrap option, default will always be true')


# Defaults
dataDir = "/data/cassandra"
logDir = "/log/cassandra"
commitLogDir = "/commitlog/cassandra"
auto_bootstrap = "true"
hostIP = "localhost"
seeds = "127.0.0.1"

args = parser.parse_args()

if args.clusterName:
	print "Setting up a node with cluster name " + args.clusterName
else:
	print "required --clusterName"
	exit(1)

clusterName = args.clusterName

if args.seeds:
	assert isinstnace(args.seeds, object)
	seeds = args.seeds

if args.hostIP:
	hostIP = args.hostIP

if args.dataDir:
	dataDIr = args.dataDIr

if args.logDir:
	logDir = args.logDir

if args.commitLogDir:
	commitLogDir = args.commitLogDir

if args.auto_bootstrap:
	auto_bootstrap = args.auto_bootstrap

rpcPort = 9160
if args.rpcPort:
	rpcPort = args.rpcPort

# Template loading and rendering

templateLoader = jinja2.FileSystemLoader(searchpath="/")
templateEnv = jinja2.Environment(loader=templateLoader)

templateVars = dict(clusterName=clusterName, seeds=seeds, rpcPort=rpcPort, hostIP=hostIP, dataDir=dataDir, logDir=logDir, commitLogDir=commitLogDir, auto_bootstrap=auto_bootstrap)

#Configure cassandra template
CASSANDRA_TEMPLATE_FILE = "/apps/scripts/cassandra-template.yaml"
cassandraTemplate = templateEnv.get_template(CASSANDRA_TEMPLATE_FILE)

renderedOutput = cassandraTemplate.render(templateVars)
with open("/apps/cassandra/conf/cassandra.yaml", "wb") as fh:
	fh.write(renderedOutput)

# Configure cassandra logback template
CASSANDRA_LOGBACK_TEMPLATE_FILE = "/apps/scripts/logback-template.xml"
cassandraLogbackTemplate = templateEnv.get_template(CASSANDRA_LOGBACK_TEMPLATE_FILE)

renderedOutputLogback = cassandraLogbackTemplate.render(templateVars)
with open("/apps/cassandra/conf/logback.yaml", "wb") as fh:
	fh.write(renderedOutputLogback)

# Starting cassandra
startCassandraCommand = '/apps/cassandra/bin/cassandra -f'
print "Starting cassandra with " + startCassandraCommand
# os.excel('/apps/cassandra/bin/cassandra', 'cassandra', '-R', '-f')
