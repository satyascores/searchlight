import org.elasticsearch.common.logging.*;
ESLogger LOGGER = ESLoggerFactory.getLogger("update_summary_event");

try{
	ctx._source.timesSeen +=1

	if (createdAt < ctx._source.firstSeenAt) {
		ctx._source.firstSeenAt = createdAt
	} else if (createdAt > ctx._source.lastSeenAt) {
		ctx._source.lastSeenAt = createdAt
	}

	if (receivedAt > ctx._source.lastUpdatedAt) {
		ctx._source.lastUpdatedAt = receivedAt
	}

	if (createdAt >= ctx._source.lastSeenAt) {
		ctx._source.eventClass = eventClass;
		ctx._source.title = title;
		ctx._source.source = source;

		if (message?.trim()) {
			ctx._source.message = message;
		}

		if (status?.trim()) {
			ctx._source.status = status;
			if (status.equalsIgnoreCase("closed")) {
				ctx._source.closedAt = createdAt;
			} else {
				ctx._source.closedAt = null;
			}
		}

		if (!sender?.isEmpty()) {
			ctx._source.sender = sender;
		}

		ctx._source.properties = properties;
		ctx._source.tags = tags;
		ctx._source.metadata = metadata;
	}
} catch(Exception e) {
	LOGGER.error(e.toString())
	LOGGER.error(e.getMessage())
	LOGGER.error(e.getStackTrace())
}