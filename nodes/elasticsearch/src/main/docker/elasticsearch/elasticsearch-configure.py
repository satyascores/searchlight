#!/usr/bin/python

import argparse, os
import jinja2

parser = argparse.ArgumentParser(description='Configure a Elasticsearch ensemble.')

parser.add_argument('--clusterName', help='Specify a unique cassandra cluster name')
parser.add_argument('--nodeName', help='Specify Elasticsearch node name - default is es-geneva-node1')
parser.add_argument('--servers', help='Specify the hostnames of servers in the ensemble of the form host:port,host:port,...., default localhost')
parser.add_argument('--dataPath', help='Specify number of shards., default 5')
parser.add_argument('--logPath', help='Specify number of replicas, default 1')
parser.add_argument('--numberOfShards', help='Specify number of shards., default 5')
parser.add_argument('--numberOfReplicas', help='Specify number of replicas, default 1')
parser.add_argument('--tcpPort', help='Specify tcp port. default is 9300')
parser.add_argument('--httpPort', help='Specify tcp port. default is 9200')
parser.add_argument('--networkHost', help='Specify hostname, default 0.0.0.0')
parser.add_argument('--minHeapSize', help='Specify minimum JVM heap size. e.g. 1g, 5g, 10g, etc., default 1g')
parser.add_argument('--maxHeapSize', help='Specify maximum JVM heap size. e.g. 1g, 5g, 10g, etc., default 1g')

nodeName='es-geneva-node1'
numberOfShards=5
numberOfReplicas=1
dataPath='/data/elasticsearch'
logPath='/logs/elasticsearch'
tcpPort=9300
httpPort=9200
networkHost='0.0.0.0'
minHeapSize='4g'
maxHeapSize='4g'

args = parser.parse_args()

if args.clusterName:
   print "Configuring ES with clusterName : "+args.clusterName
   clusterName=args.clusterName
else:
	print "required --clusterName"
	exit(1)

if args.nodeName:
	nodeName=args.nodeName

if args.numberOfShards:
	numberOfShards=args.numberOfShards

if args.numberOfReplicas:
	numberOfReplicas=args.numberOfReplicas

if args.dataPath:
	dataPath=args.dataPath

if args.logPath:
	logPath=args.logPath

if args.tcpPort:
	tcpPort=args.tcpPort

if args.httpPort:
	httpPort=args.httpPort

if args.networkHost:
	networkHost=args.networkHost

if args.minHeapSize:
	minHeapSize=args.minHeapSize

if args.maxHeapSize:
	maxHeapSize=args.maxHeapSize

server=['localhost']
if(args.servers):
	servers=args.servers.split(",")

templateVars = {'clusterName': clusterName, 'nodeName':nodeName, 'numberOfShards': numberOfShards, 'numberOfReplicas': numberOfReplicas, 'dataPath': dataPath, 'logPath': logPath, 'tcpPort': tcpPort, 'httpPort': httpPort, 'servers': servers, 'networkHost': networkHost, 'minHeapSize': minHeapSize, 'maxHeapSize': maxHeapSize}

# Template loading and rendering
templateLoader = jinja2.FileSystemLoader( searchpath="/" )
templateEnv = jinja2.Environment( loader=templateLoader )

TEMPLATE_FILE = "/apps/elasticsearch/elasticsearch-template.yml"
template = templateEnv.get_template( TEMPLATE_FILE )

print 'Creating elasticsearch.yml'
ES_VERSION="elasticsearch-2.1.2"

renderedOutput = template.render( templateVars)
with open("/apps/elasticsearch/"+ES_VERSION+"/config/elasticsearch.yml", "wb") as fh:
	fh.write(renderedOutput)

print 'Creating logging.yml'
LOGGING_TEMPLATE_FILE = "/apps/elasticsearch/logging-template.yml"
loggingTemplate = templateEnv.get_template( LOGGING_TEMPLATE_FILE )
renderedOutput = loggingTemplate.render( templateVars )

with open("/apps/elasticsearch/"+ES_VERSION+"/config/logging.yml", "wb") as fh:
	fh.write(renderedOutput) 

print 'Starting Elasticsearch node '+nodeName+' in cluster '+clusterName
startElasticsearchCommand='/apps/elasticsearch/'+ES_VERSION+'/bin/elasticsearch'
