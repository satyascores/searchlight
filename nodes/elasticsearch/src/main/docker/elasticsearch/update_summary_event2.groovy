import org.elasticsearch.common.logging.*;
ESLogger LOGGER = ESLoggerFactory.getLogger("update_summary_event");

try{

	if (firstSeenAt < ctx._source.firstSeenAt) {
		ctx._source.firstSeenAt = firstSeenAt
	} 

	if (lastSeenAt > ctx._source.lastSeenAt) {
		ctx._source.lastSeenAt = lastSeenAt
	}

	if (lastUpdatedAt > ctx._source.lastUpdatedAt) {
		ctx._source.lastUpdatedAt = lastUpdatedAt
	}

	if (lastSeenAt >= ctx._source.lastSeenAt) {
		ctx._source.eventClass = eventClass
		ctx._source.title = title
		ctx._source.source = source

		if (!sender?.isEmpty()) {
			ctx._source.sender = sender
		} else {
			ctx._source.remove("sender")
		}

		String tmp

		tmp = severity?.trim()
		if (tmp) {
			ctx._source.severity = tmp
		} else {
			ctx._source.remove("severity")
		}

		tmp = message?.trim()
		if (tmp) {
			ctx._source.message = tmp
		} else {
			ctx._source.remove("message")
		}

		tmp = status?.trim()
		if (tmp) {
			ctx._source.status = tmp
		} else {
			ctx._source.remove("status")
		}

		if (closedAt) {
			ctx._source.closedAt = closedAt
		} else {
			ctx._source.remove("closedAt")
		}

		ctx._source.properties = properties
		ctx._source.tags = tags
		ctx._source.metadata = metadata
	}

	ctx._source.timesSeen += timesSeen

} catch(Exception e) {
	
	LOGGER.error(e.toString())
	LOGGER.error(e.getMessage())
	LOGGER.error(e.getStackTrace())
}